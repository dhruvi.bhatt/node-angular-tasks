import { Pipe, PipeTransform } from '@angular/core';
import { User } from './crud-api/users';

@Pipe({
  name: 'searchfilter'
})
export class SearchfilterPipe implements PipeTransform {

  transform(users: User[],searchvalue: string): User[] {
    if(!users || !searchvalue){
      return users;
    }
    return users.filter(User => 
    User.first_name.toLocaleLowerCase().includes(searchvalue.toLocaleLowerCase()));
  }

}
