import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login : any =  FormGroup;
  isFormSubmitted = false;

  constructor(
    public fb : FormBuilder,
    public router: Router,
  ) { }

  get f(){
    return this.login.controls;
  }


  LoginForm(){

    // console.log(this.login.value.email);
    
    const getvalue = JSON.parse(localStorage.getItem('data') || '{}');
    console.log(getvalue.email)
    console.log(getvalue.pass)

    const loginsuccess = ( this.login.value.email === getvalue.email && this.login.value.pass === getvalue.pass )

    if (!loginsuccess){
      console.log("Email And Password Are Not Correct");
      return
    }
    else{
      console.log("Login Successfully");
      
      this.router.navigateByUrl('/crud-api/view')
    }
  }
  ngOnInit(): void {
    this.login = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      pass: new FormControl('', [Validators.required, Validators.minLength(6)])
     
    })
  }
}