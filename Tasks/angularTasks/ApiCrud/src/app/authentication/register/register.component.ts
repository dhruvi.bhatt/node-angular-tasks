import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm : any =  FormGroup;
  isFormSubmitted = false;

  constructor(
    public fb : FormBuilder,
    public router: Router,
  ) { }


  get f(){
    return this.registerForm.controls;
  }

  RegisterForm(){

    localStorage.setItem('data', JSON.stringify(this.registerForm.value))
    console.log(this.registerForm.value);
    const getvalue = (localStorage.getItem('data'));
    console.log(getvalue);
    this.router.navigateByUrl('/authentication/login')
    }
  ngOnInit(): void {
    this.registerForm = this.fb.group({
      uname: new FormControl('', [Validators.required, Validators.minLength(3)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      pno: new FormControl('', [Validators.required, Validators.minLength(10)]),
      pass: new FormControl('', [Validators.required, Validators.minLength(6)] )
    })
  }
}
