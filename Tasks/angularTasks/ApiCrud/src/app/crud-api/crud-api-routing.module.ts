import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { ViewComponent } from './view/view.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  { path: 'crud-api', redirectTo: 'crud-api/index', pathMatch: 'full'},
  { path: 'crud-api/index', component: IndexComponent },
  { path: 'crud-api/view', component: ViewComponent },
  { path: 'crud-api/create', component: CreateComponent },
  { path: 'crud-api/edit', component: EditComponent } 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudAPIRoutingModule { }
