import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudApiService } from '../crud-api.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  userForm!: FormGroup;
  isFormSubmitted = false;

  constructor(public fb: FormBuilder, public router: Router, public crudapiservice:CrudApiService) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      
      first_name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      last_name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      avtar : new FormControl('', [Validators.required, Validators.minLength(5)])
    }),
    this.userForm = this.fb.group({
      first_name:[''],
      last_name:[''],
      email:[''],
      avtar:['']
    })
  }
  get f(){
    return this.userForm.controls;
  }
  SubmitData()
    {
      this.crudapiservice.create(this.userForm.value).subscribe(res => 
      {
        
        console.log('Data Added!')
  
        
        this.router.navigateByUrl('/crud-api/view')
      }
    )}


}
