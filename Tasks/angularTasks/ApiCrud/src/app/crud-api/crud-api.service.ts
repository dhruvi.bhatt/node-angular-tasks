import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
    
import {  Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {User} from './users'
@Injectable({
  providedIn: 'root'
})
export class CrudApiService {

  private apiServer = "http://localhost:3000";
  httpOptions = {
    headers: new HttpHeaders ({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) {  }

  create(user: any): Observable<User> {
    return this.httpClient.post<User>(this.apiServer + '/users/', JSON.stringify(user), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
 
  getById(id: any): Observable<User> {
    return this.httpClient.get<User>(this.apiServer + '/users/' + id)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getAll(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.apiServer + '/users/')
    .pipe(
      catchError(this.errorHandler)
    )
  }

  update(id:any, user:any): Observable<User> {
    return this.httpClient.put<User>(this.apiServer + '/users/' + id, JSON.stringify(user), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  
  delete(id:any){    
    return this.httpClient.delete<User>(this.apiServer + '/users/' +  id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
 }

}
