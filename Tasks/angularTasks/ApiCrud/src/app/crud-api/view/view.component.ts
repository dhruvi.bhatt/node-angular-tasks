import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder } from '@angular/forms';
import { User } from '../users';
import { Router } from '@angular/router';

import { CrudApiService } from '../crud-api.service';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  users: User[] = [];
 
  searchvalue: any;
  page: number = 1 ;
  itemsPerPage: number = 6;
  totalItems : any; 
  editUserForm!: FormGroup;


  constructor(public fb: FormBuilder, public crudapiservice: CrudApiService, public router: Router, public modalservice: NgbModal, public httpclient:HttpClient) { }

  ngOnInit(): void {
   this.GetUsers();

  }

  GetUsers(){
    this.httpclient.get(`http://localhost:3000/users?page=${this.page}&size=${this.itemsPerPage}`).subscribe((data: any)=> {
      console.log("page", this.page);
      console.log("itemsPerPage", this.itemsPerPage);
      console.log(data);
      this.users = data;
      console.log(this.users);
      
      this.page = 0;
     
    }) 
  }
  createRute(){
    this.router.navigateByUrl('/crud-api/create')
  }

}


