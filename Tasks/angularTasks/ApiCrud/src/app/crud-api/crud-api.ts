export interface CrudApi {
    id: number;
    name: string;
    category: string;
}
