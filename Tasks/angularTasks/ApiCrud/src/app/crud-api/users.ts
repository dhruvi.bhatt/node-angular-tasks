export interface User {
    id: number;
    last_name: string;
    first_name: string;
    email:string;
    avtar:string;
}
