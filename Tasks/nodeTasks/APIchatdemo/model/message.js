const mongoose = require("mongoose");
const UserData = require("../model/user");
const MessageSchema = new mongoose.Schema({
    userid: {
        type: String
    },
    msgid:{
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ['read','unread'],
        default: 'read'
    },
    created_date: {
        default: Date.now(), 
        type: String
    }
});

const Message = mongoose.model("Message", MessageSchema);
module.exports = Message;