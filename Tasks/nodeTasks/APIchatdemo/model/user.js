const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    userid: {
        type: String,
        // required: true
    },
    username: {
        type: String,
        // required: true
    },
    email:{
        type: String
    },
    password:{
        type: String,
        // required: true
    }
});

const UserData = mongoose.model('UserData', UserSchema);
module.exports = UserData;