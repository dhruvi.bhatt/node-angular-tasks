const express = require("express");
const app = express();
const db = require('./config/db');

app.use(express.json());

app.use("/", require('./routes/userroute'));

app.use("/msg", require('./routes/messageroute'));

const port = 3000;
app.listen(port, () => {
    console.log(`server running on ${port}`);
})


