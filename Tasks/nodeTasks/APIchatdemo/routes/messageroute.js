const express = require('express')

const router = express.Router();

const messagecontroller = require("../controller/MessageController");

router.post("/addmessage", messagecontroller.messagepost);
router.get("/allmessages", messagecontroller.msgList);
router.get("/messagebyid/msgid=", messagecontroller.msgById );
router.delete("/msgdelete/msgid=", messagecontroller.msgdelete);

module.exports = router;



