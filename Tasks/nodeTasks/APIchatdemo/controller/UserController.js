const UserData = require("../model/user");

exports.register = async(req, res) => {
    const data = new UserData({
        userid: "USER" + Date.now(Math.random()),
        username: req.body.username,  
        email: req.body.email,
        password: req.body.password
    });
    try {
        const dataToSave = await data.save();
        console.log(dataToSave, "dataToSave")
        res.status(200).json(dataToSave)
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
}

exports.login = async (req, res) => {
    const logindata = await UserData.findOne ({
        email: req.body.email,
        password: req.body.password
    });
    try{
        res.status(200).json("Login successfully")
    }
    catch (error){
        res.status(400).json({ message: error.message })
    }
}

