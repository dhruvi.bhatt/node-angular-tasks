const Message = require("../model/message");
const UserData = require("../model/user");

exports.messagepost = async(req, res)=> {
   
    var user_ids = '';


    UserData.find(async function(err, docs) {
        if (err) {
            console.log(err, "User id not found");
        } else {
            user_ids = docs[0].userid;
            const data = new Message({
                msgid: "MSG" + Date.now(Math.random()),
                message: req.body.message,
                userid: req.body.userid,
            });
            try {
                console.log("user_ids",user_ids)
                console.log(req.body.userid,"req.body.userid");
                if (user_ids != req.body.userid) {
                    const dataToSave = await data.save();
                    console.log(dataToSave, "dataToSave");
                    res.status(200).json(dataToSave);
                } else {
                    res.status(400).json("user is does not exist");
                }

            } catch (error) {
                res.status(400).json({ message: error.message })
            }

        }  
    });
}

exports.msgList = async(req, res) => {    
    try {
        const msglist = await Message.find();
        res.json(msglist)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
    
}


exports.msgById = async(req, res) => {
    try {
        const getmsgid = await Message.findById(req.params.msgid);
        res.json(getmsgid)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}


exports.msgdelete = async(req, res) => {
    try {
        const msgdel = await Message.findByIdAndDelete(req.body.msgid)
        res.json(msgdel)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}